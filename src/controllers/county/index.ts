import * as express from 'express'
import * as counties from '../../data.json'

const router = express.Router()

router.get('/suggest', (req, res) => {
    const { q } = req.query
    if (!q)
        return res.send(400)
    const [x, state] = (<string>q).toLowerCase().split(', ')
        
    let results = []
    for (const county of counties) {
        if (results.length == 5) break
        if (state) {
            const name = x
            if (county.name.toLowerCase().startsWith(name)
            && county.state.toLowerCase().startsWith(state)) {
                results.push(county)
            }
        }
        else if (county.name.toLowerCase().startsWith(x)
        || county.state.toLowerCase().startsWith(x)) {
            results.push(county)
        }
    }
    res.send(results)
})

export default router