import * as express from 'express'
import countyController from '@controllers/county'

const app = express()
const port = 3000

app.use('/', countyController)

app.listen(process.env.port || port, () => {
    console.log(`Listening at port ${port}`)
})

export default app