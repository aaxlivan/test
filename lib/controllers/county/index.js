"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var counties = require("../../data.json");
var router = express.Router();
router.get('/suggest', function (req, res) {
    var q = req.query.q;
    if (!q)
        return res.send(400);
    var _a = q.toLowerCase().split(', '), x = _a[0], state = _a[1];
    var results = [];
    for (var _i = 0, counties_1 = counties; _i < counties_1.length; _i++) {
        var county = counties_1[_i];
        if (results.length == 5)
            break;
        if (state) {
            var name_1 = x;
            if (county.name.toLowerCase().startsWith(name_1)
                && county.state.toLowerCase().startsWith(state)) {
                results.push(county);
            }
        }
        else if (county.name.toLowerCase().startsWith(x)
            || county.state.toLowerCase().startsWith(x)) {
            results.push(county);
        }
    }
    res.send(results);
});
exports.default = router;
//# sourceMappingURL=index.js.map