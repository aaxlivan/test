"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var county_1 = require("./controllers/county");
var app = express();
var port = 3000;
app.use('/', county_1.default);
app.listen(process.env.port || port, function () {
    console.log("Listening at port " + port);
});
exports.default = app;
//# sourceMappingURL=app.js.map