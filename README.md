## Getting Started

1. Clone or download the repo (Skip this part if you already have done so).
```
git clone https://gitlab.com/aaxlivan/test.git
```
2. Install dependencies
```
cd test
npm install
```
3. Build application
```
npm run build
```
4. Run application
```
npm start
```

## Run with docker
If you have not installed docker please [install](https://docs.docker.com/get-docker/) first.
1. Run using docker-compose
```
docker-compose up
```

## Test
Test with jest & supertest
```
npm run test
```
Test with SoapUI \
*Read docs* [here](https://www.soapui.org/getting-started/)

## Documentation
### API Documentation
[See here](https://gitlab.com/aaxlivan/test/-/blob/master/spec.yaml)

Open with Swagger Editor
1. Download [spec.yaml](https://gitlab.com/aaxlivan/test/-/blob/master/spec.yam)
2. Open Swagger Editor [here](https://editor.swagger.io/)
3. Import spec.yaml



