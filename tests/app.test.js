const app = require('../lib/app').default
const request = require('supertest')

test('get suggest-counties without query', async () => {
    const res = await request(app).get('/suggest')
    expect(res.status).toBe(400)
})

test('get suggest-counties with query q=cowl', async () => {
    const res = await request(app).get('/suggest?q=cowl')
    expect(res.status).toBe(200)
    expect(res.body).toEqual([{"fips":"20035","state":"KS","name":"Cowley"},{"fips":"53015","state":"WA","name":"Cowlitz"}])
})

test('get suggest-counties with query q=cowlitz, wa' /**name, state */, async () => {
    const res = await request(app).get('/suggest?q=cowlitz, wa')
    expect(res.status).toBe(200)
    expect(res.body).toEqual([{"fips":"53015","state":"WA","name":"Cowlitz"}])
})